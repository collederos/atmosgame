#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CheatManager.h"

#include "AtmosInventoryCheats.generated.h"

class ULyraInventoryManagerComponent;

UCLASS(NotBlueprintable)
class UAtmosInventoryCheats final : public UCheatManagerExtension
{
	GENERATED_BODY()

public:
	UAtmosInventoryCheats();
	
	UFUNCTION(Exec, BlueprintAuthorityOnly)
	void AddItemToInv(const FString& ItemDefinitionAssetName) const;

private:
	ULyraInventoryManagerComponent* GetInventoryManager() const;
};
