﻿#include "Inventory/AtmosInventoryCheats.h"

#include "LyraInventoryItemDefinition.h"
#include "GameFramework/GameStateBase.h"
#include "Inventory/LyraInventoryManagerComponent.h"
#include "GameFramework/Character.h"
#include "LyraGame/System/LyraDevelopmentStatics.h"

class ULyraEquipmentManagerComponent;

UAtmosInventoryCheats::UAtmosInventoryCheats()
{
#if UE_WITH_CHEAT_MANAGER
		if (HasAnyFlags(RF_ClassDefaultObject))
		{
			UCheatManager::RegisterForOnCheatManagerCreated(FOnCheatManagerCreated::FDelegate::CreateLambda(
				[](UCheatManager* CheatManager)
				{
					CheatManager->AddCheatManagerExtension(NewObject<ThisClass>(CheatManager));
				}));
		}
#endif
}

void UAtmosInventoryCheats::AddItemToInv(const FString& ItemDefinitionAssetName) const
{
#if UE_WITH_CHEAT_MANAGER
	if (ULyraInventoryManagerComponent* InventoryManager = GetInventoryManager())
	{
		if (const auto ItemDefClass{
			ULyraDevelopmentStatics::FindClassByShortName<ULyraInventoryItemDefinition>(ItemDefinitionAssetName)
		})
		{
			InventoryManager->Cheat_AddItemDefinition(ItemDefClass, 1);
		}
	}
#endif
}

ULyraInventoryManagerComponent* UAtmosInventoryCheats::GetInventoryManager() const
{
	if (APlayerController* PC = GetPlayerController())
	{
		return PC->FindComponentByClass<ULyraInventoryManagerComponent>();
	}

	return nullptr;
}
